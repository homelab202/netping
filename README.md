# netping

A simple tool to ping all hosts in a given range.

# Contents

1. [License](#License)
2. [Dependencies](#Dependencies)
3. [Installation](Installation)
4. [Usage](Usage)
5. [Examples](Examples)

# License 

GNU GENERAL PUBLIC LICENSE Version 3 see [LICENSE](./LICENSE)

# Dependencies

- [ping](https://github.com/iputils/iputils)
- [host](https://github.com/isc-projects/bind9)

# Installation

`install --mode 0755 ./netping.sh /usr/sbin/netping` 

# Usage

``` SHELL
usage: netscan [OPTIONS]

OPTIONS:
	-i=*|--interface=*	interface to scan
	-n=*|--network=*	network to scan
	-l=*|--lower=*		lower IP range
	-u=*|--upper=*		upper IP range
	-h|--help			print this help
```

# Examples

`./netping.sh --network=10.10.0. `
`./netping.sh --interface=eth0 --lower=20 --upper=29`
