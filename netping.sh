#!/bin/bash

set -eo pipefail

OPT_HELP="\
usage: netscan.sh [OPTIONS]

OPTIONS:
	-i=*|--interface=*	interface to scan (default: wlan0)
	-n=*|--network=*	network to scan
	-l=*|--lower=*		lower IP range (default: 1)
	-u=*|--upper=*		upper IP range (default: 254)
	-h|--help		print this help
"

OPT_LOWER=1
OPT_UPPER=254
OPT_INTERFACE=wlan0

for o in "$@"
do
	case $o in
		-i=*|--interface=*)	OPT_INTERFACE="${o#*=}";			shift	;;
		-n=*|--network=*)	OPT_NETWORK="${o#*=}";				shift	;;
		-l=*|--lower=*)		OPT_LOWER="${o#*=}";				shift	;;
		-u=*|--upper=*)		OPT_UPPER="${o#*=}";				shift	;;
		-h|--help)		echo -e "$OPT_HELP";				exit 1	;;
		-*|--*)			echo -e "unknown option: $0\n$OPT_HELP";	exit 1	;;
		*)										;;
	esac
done

oneping() {
	a=$(ping $1 -c 1 -W 1 | tail -n 1)
	if [[ "$a" =~ ^rtt* ]]; then
		host $1
	fi
}

if [ ! -z ${OPT_NETWORK+x} ]; then
	net=$OPT_NETWORK	
else
	ip=$(/sbin/ip -o -4 addr list $OPT_INTERFACE| awk '{print $4}' | cut -d/ -f1)
	net=$(cut -d '.' -f1-3 <<< $ip).
fi

for i in $(seq $OPT_LOWER $OPT_UPPER) ; do
	oneping $net$i &
done
wait
